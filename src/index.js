import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import Table from './table'
import AddRecord from './addRecord'
import EditRecord from './editRecord'
import ErrorMessage from './errorMessage'
import '../css/main.css'

const el = document.getElementById('root-node')

class App extends Component {
  constructor() {
    super()
    this.state = {
      db: [
        { id: 1, name: 'Bruce Wayne' },
        { id: 2, name: 'Alfred Pennyworth' },
        { id: 3, name: 'Harvey Dent' },
        { id: 4, name: 'James Gordon' },
        { id: 5, name: 'The Joker' },
        { id: 6, name: 'Rachel Dawes' },
        { id: 7, name: 'Lucius Fox' },
        { id: 8, name: 'Sal Maroni' }
      ],
      columns: ['ID', 'Name'],
      newrecord: '',
      nextId: 9,
      editVisible: false,
      editedIndex: null,
      editedRecord: '',
      error: ''
    }
  }

  componentDidMount() {
    fetch(endpoint)
      .then(response => response.json())
      .then(data => this.setState({ db: data }))
  }

  handleRowDelete = (id, e) => {
    e.preventDefault()
    fetch(endpoint, {
      method: 'DELETE',
      body: JSON.stringify({ entry: id }),
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(
      response => {
        if (response.status === 200) {
          this.setState({
            db: this.state.db.filter(entry => entry.id !== id),
            error: ''
          })
        }
        return response.text()
      },
      err => {
        this.setState({ error: err.message })
      }
    )
  }

  handleRowAddChange = e => {
    this.setState({ newrecord: e.target.value })
  }

  handleRowAdd = e => {
    e.preventDefault()
    const { newrecord, nextId } = this.state
    if (!newrecord.length) {
      return
    }
    /* const entry = {
      id: nextId,
      name: newrecord
    } */
    fetch(endpoint, {
      method: 'POST',
      body: JSON.stringify({ name: newrecord }),
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(response => response.json())
      .then(data =>
        this.setState(prevState => ({
          db: prevState.db.concat(data),
          newrecord: ''
          // nextId: prevState.nextId + 1
        }))
      )
  }

  handleRowEditChange = e => {
    this.setState({ editedRecord: e.target.value })
  }

  handleRowEditing = (id, e) => {
    const index = this.state.db.findIndex(entry => entry.id === id)
    if (!this.state.editVisible) {
      this.setState({
        editVisible: true,
        editedIndex: index,
        editedRecord: this.state.db[index].name
      })
    }
  }

  handleRowEditingExit = e => {
    e.preventDefault()
    this.setState({
      editVisible: false,
      editedIndex: null,
      editedRecord: ''
    })
  }

  handleRowEditSave = e => {
    e.preventDefault()
    const { editedRecord, editedIndex, db } = this.state
    if (!editedRecord.length) {
      return
    }
    // const newDb = db
    // newDb[editedIndex].name = editedRecord
    fetch(endpoint, {
      method: 'PUT',
      body: JSON.stringify({ name: editedRecord }),
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(response => response.json())
      .then(data =>
        this.setState({
          db: data,
          editVisible: false
        })
      )
  }

  render() {
    return (
      <div>
        <ErrorMessage message={this.state.error} />
        <Table
          data={this.state.db}
          cols={this.state.columns}
          onRowDelete={this.handleRowDelete}
          onRowEdit={this.handleRowEditing}
        />
        <AddRecord
          inputText={this.state.newrecord}
          onChange={this.handleRowAddChange}
          onAdd={this.handleRowAdd}
        />
        <EditRecord
          inputText={this.state.editedRecord}
          onChange={this.handleRowEditChange}
          onEdit={this.handleRowEditSave}
          onExit={this.handleRowEditingExit}
          isVisible={this.state.editVisible}
        />
      </div>
    )
  }
}

ReactDOM.render(<App />, el)
