import React from 'react'
import PropTypes from 'prop-types'

const EditRecord = ({ isVisible, inputText, onEdit, onChange, onExit }) =>
  isVisible && (
    <div>
      <form onSubmit={onEdit}>
        <fieldset>
          <legend>Upravit zaznam:</legend>
          <input value={inputText} onChange={onChange} />
          <input type="submit" className="btn btn-success" value="Ok" />
          <input
            type="button"
            onClick={onExit}
            className="btn btn-secondary"
            value="Zrusit"
          />
        </fieldset>
      </form>
    </div>
  )

EditRecord.propTypes = {
  onEdit: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  onExit: PropTypes.func.isRequired,
  inputText: PropTypes.string,
  isVisible: PropTypes.bool.isRequired
}

export default EditRecord
