import React from 'react'
import PropTypes from 'prop-types'
import { TableRow, TableCellHead } from './tableAssets'

export const TableHead = ({ columns }) => (
  <thead>
    <TableRow>
      {columns.map(column => (
        <TableCellHead key={column}>{column}</TableCellHead>
      ))}
    </TableRow>
  </thead>
)

TableHead.propTypes = {
  columns: PropTypes.arrayOf(PropTypes.string).isRequired
}
