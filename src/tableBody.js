import React from 'react'
import PropTypes from 'prop-types'
import { TableRow, TableCell } from './tableAssets'

export const TableBody = ({ data, onRowDelete, onRowEdit }) => (
  <tbody>
    {data.map(entry => (
      <TableRow key={entry.id}>
        <TableCell>{entry.id}</TableCell>
        <TableCell>{entry.name}</TableCell>
        <TableCell>
          <button
            className="btn btn-danger"
            onClick={e => onRowDelete(entry.id, e)}
          >
            Smazat
          </button>
        </TableCell>
        <TableCell>
          <button
            className="btn btn-warning"
            onClick={e => onRowEdit(entry.id, e)}
          >
            Upravit
          </button>
        </TableCell>
      </TableRow>
    ))}
  </tbody>
)

TableBody.propTypes = {
  data: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string
    })
  ).isRequired,
  onRowDelete: PropTypes.func.isRequired,
  onRowEdit: PropTypes.func.isRequired
}
