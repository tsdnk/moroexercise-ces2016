import React from 'react'
import PropTypes from 'prop-types'
import { TableHead } from './tableHead'
import { TableBody } from './tableBody'

const Table = ({ data, cols, onRowDelete, onRowEdit }) => (
  <table className="table table-striped">
    <TableHead columns={cols} />
    <TableBody data={data} onRowDelete={onRowDelete} onRowEdit={onRowEdit} />
  </table>
)

Table.propTypes = {
  data: PropTypes.array.isRequired,
  cols: PropTypes.array.isRequired,
  onRowDelete: PropTypes.func.isRequired,
  onRowEdit: PropTypes.func.isRequired
}

export default Table
