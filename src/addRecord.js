import React from 'react'
import PropTypes from 'prop-types'

const AddRecord = ({ inputText, onAdd, onChange }) => (
  <div>
    <form onSubmit={onAdd}>
      <fieldset>
        <legend>Pridat zaznam:</legend>
        <input type="text" value={inputText} onChange={onChange} />
        <input type="submit" className="btn btn-primary" value="Pridat" />
      </fieldset>
    </form>
  </div>
)

AddRecord.propTypes = {
  onAdd: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  inputText: PropTypes.string
}

export default AddRecord
