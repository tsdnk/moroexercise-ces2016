import React from 'react'

export const TableRow = ({ children }) => <tr>{children}</tr>

export const TableCell = ({ children }) => <td>{children}</td>

export const TableCellHead = ({ children }) => <th>{children}</th>
