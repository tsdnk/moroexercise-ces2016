# UI část #

### Fáze 1 (jednoduché prostředí) ###
* Vytvoř React aplikaci o jedné komponentě
* Použij starter kit a postupuj dle návodu (https://facebook.github.io/react/docs/getting-started.html#quick-start-without-npm)
* Doporučený editor SublimeText (http://www.nitinh.com/2015/02/setting-sublime-text-react-jsx-development/) nebo IntelliJ IDEA
* Tvým cílem je, aby komponenta v renderu vypsala tabulku uživatelů uložených v proměnné (nebo staticky/v metodě komponenty)
* Styly řeš normálním .css souborem includovaným v index.html
* stylování není předmětem tohoto příkladu, ale aplikace by mohla mít nějaké základní styly - optional

### Fáze 2 (komponenty) ###
* Rozděl aplikaci do více komponent (souborů)
* index.html, který renderuje hlavní komponentu
* Hlavní komponenta aplikace, která obsahuje data ve svém stavu. Data se do něj naplní v konstruktoru
* Komponenta tabuky, které budou data dávána přes props
* nezapomeň na PropTypes
* Tabulka bude složená z komponent pro hlavičku, tělo, řádky, buňky
* vše vhodně poskládej
* dobrý zdroj na pochycení základních best practices je https://camjackson.net/post/9-things-every-reactjs-beginner-should-know
* Data postačí pole dvojic id, name. Případně jakákoli další data plánuješ posílat z backend části pro uživatele

### Fáze 3 (interaktivita) ###
* Přidej sloupec do tabulky. V něm bude tlačítko "Smazat", které smaže daný řádek z tabulky
* smaže daný záznam ze stavu hlavní komponenty (následkem čehož se překreslí komponenty)
* pravděpodobně budeš chtít, aby komponenta tabulky měla callback typu "onRowDeleted", který si doposíláš přes props na
* patříčná místa a button
* Pod tabulku přidej textfield a tlačítko "Přidat", které přidají do tabulky záznam se jménem vyplněném v políčku
* jako v předchozám bodě, přidej vše do dat ve stavu hlavní komponenty
* textfield bude komponenta, zaobalující <input type="text">
* Přidej do tabulky sloupec s tlačítkem "Upravit"
* po kliknutí na něj se pod tabulkou zobrazí komponenta s políčkem (políčky) pro editaci aktuálního řádku
* vedle políček bude tlačítko OK, které daný záznam potvrdí
* případně i Cancel/Close na zrušení editace - optional
* po potvrzení se aktualizují data v tabulce (stavu hlavní komponenty)

### Fáze 4 (komunikace se serverem) ###
* Změň načítání dat
* udělej metodu v hlavní komponentě, která ze serveru dotáhne seznam uživatelů a uloží do stavu
* tato metoda se bude volat v componentDidMount (http://busypeoples.github.io/post/react-component-lifecycle/)
* Změň metodu pro mazání v hlavní komponentě
* metoda pošle požadavek na smazání na server a až v případě success, smaže ze stavu
* Změň metodu na přidání uživatele
* pošle požadavek s vyplněnými daty (name) na server
* uloží do stavu výsledek volání na serveru (server by měl vrátit přidanou entitu)
* Změň metodu na editaci
* po kliku na OK při editaci se pošle request na server
* do stavu se uloží výsledek volání serveru
* Můžeš přidat indikátor chyby ze serveru - optional
* přidej komponentu, která vypisuje error hlášku, která jí přijde z props
* ta bude uložená ve stavu hlavní komponenty
* metody na načtení/přidání/mazání budou tento error ve stavu plnit v případě chyby ze serveru, mazat v případě success
* pro nové prohlížeče lze na ajax použít FETCH API https://github.github.io/fetch/
