var path = require('path');
const webpack = require('webpack');

const DEV = process.env.NODE_ENV !== 'production';

module.exports = {
  devtool: DEV ? 'cheap-module-source-map' : 'source-map',
  entry: './src/index.js',
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'bundle.js',
    publicPath: 'http://localhost:3000/static/',
  },
  module: {
    loaders: [{
      test: /\.js$/,
      loader: 'babel-loader',
      exclude: /(node_modules)/,
    },{
      test: /\.css/,
      loader: 'style-loader!css-loader',
    }]
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify(DEV ? 'development' : 'production'),
      },
    }),
    DEV && new webpack.optimize.UglifyJsPlugin({
      compress: {
        screw_ie8: true,
        warnings: false,
      },
      mangle: {
        screw_ie8: true,
      },
      output: {
        comments: true,
        screw_ie8: true,
      },
    }),
    DEV && new webpack.optimize.AggressiveMergingPlugin(),
  ].filter(Boolean),
};